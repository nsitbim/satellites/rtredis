# redisSet


**Sets a redis key**

| Key               | Val                                      |
| ----------------- | ---------------------------------------- |
| Category          | \redis\routines                          |
| type              | Parallel routine                         |
| last modification | 2020/08/20 09:28:32 isadmin              |
| checksum          | ab156c28fdfe2ab8c951160d6abdcde858bd3b35 |




NSITBIM - 20200820 - Init

Sets the value associated with the key argument.
TTL can be set with the option argument.

## Dependencies

* C redis library : https://github.com/redis/hiredis
Must have been compiled and installed

* libRtRedis.so must have been compiled and placed in the LD_LIBRARY_PATH:
/opt/IBM/InformationServer/Server/PXEngine/user_lib/

