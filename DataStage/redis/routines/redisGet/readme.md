# redisGet


**Retrieve a key from redis**

| Key               | Val                                      |
| ----------------- | ---------------------------------------- |
| Category          | \redis\routines                          |
| type              | Parallel routine                         |
| last modification | 2020/08/20 09:27:53 isadmin              |
| checksum          | 680056e3a9dcbb61518775b2211536e76fb04eda |




NSITBIM - 20200820 - Init

Returns the value associated with the key argument.

## Dependencies

* C redis library : https://github.com/redis/hiredis
Must have been compiled and installed

* libRtRedis.so must have been compiled and placed in the LD_LIBRARY_PATH:
/opt/IBM/InformationServer/Server/PXEngine/user_lib/

