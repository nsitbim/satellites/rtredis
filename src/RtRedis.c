#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <hiredis.h>
#include <unistd.h>

const char *hostname = getenv("REDIS_HOST");
const char *port = getenv("REDIS_PORT");
const char *debug = getenv("REDIS_DEBUG");
const redisContext *context = redisConnect(hostname, atoi(port));


int redisSet(char *key, char *value, char *option)
{
  if (key == NULL || value == NULL)
  {
    return 0;
  }
  redisReply *reply;
  if (context == NULL || context->err)
  {
    if (context)
    {
      printf("Error: %s\n", context->errstr);
    }
    else
    {
      printf("Can't allocate redis context\n");
    }
  }
  char cmd[] = "SET ";
  strcat(cmd, key);
  strcat(cmd, " ");
  strcat(cmd, value);
  if (option != NULL) {
    strcat(cmd, " ");
    strcat(cmd, option);
  }
  if (debug)
  {
    printf("%s\n", cmd);
  }
  reply = redisCommand(context, cmd);
  freeReplyObject(reply);
  return 0;
}

char *redisGet(char *key)
{
  if (key == NULL)
  {
    return 0;
  }
  if (strlen(key) == 0)
  {
    return 0;
  }

  redisReply *reply;
  if (context == NULL || context->err)
  {
    if (context)
    {
      printf("Error: %s\n", context->errstr);
    }
    else
    {
      printf("Can't allocate redis context\n");
    }
  }

  char cmd[] = "GET ";
  strcat(cmd, key);
  if (debug)
  {
    printf("%s\n", cmd);
  }
  reply = redisCommand(context, cmd);
  if (reply->type != REDIS_REPLY_STRING)
  {
    printf("%s\n", "key not found");
    return 0;
  }
  if (debug)
  {
    printf("-->%s\n", reply->str);
  }
  char *result = (char *)malloc(strlen(reply->str) + 1);
  strcpy(result, reply->str);
  freeReplyObject(reply);
  return result;
}
