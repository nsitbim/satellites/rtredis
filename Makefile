ROUTINE = RtRedis
CODESRC = ./src/$(ROUTINE).c
TARGETOBJ= ./obj/$(ROUTINE).o
TARGETLIB= ./lib/lib$(ROUTINE).so
DEPS = ./lib/hiredis

INCLUDES = -I$(DEPS)
LIBS = -L$(DEPS)

CC=g++
CCOPTIONS=-c -O -fPIC -Wno-deprecated  -m64 -mtune=generic -mcmodel=small -fpermissive
LIBCC=g++
LDOPTIONS=-shared -m64 -lhiredis


.SUFFIXES: .c .o

# ---------------------------------------------------------------------
# Rules 
# ---------------------------------------------------------------------

all: $(TARGETLIB)
	
$(TARGETLIB):	$(TARGETOBJ)
	$(LIBCC) $(LDOPTIONS)  $(LIBS) $(TARGETOBJ) -o $(TARGETLIB)

$(TARGETOBJ): $(CODESRC)
	$(CC) $(CCOPTIONS)  $(INCLUDES)  $(LIBS)  -c $(CODESRC) -o $(TARGETOBJ) 

clean:
	@rm -f $(TARGETOBJ) $(TARGETLIB)

# ---------------------------------------------------------------------
# End of makefile
# ---------------------------------------------------------------------
