# RtRedis

RtRedis is a library for DataStage parallel routines allowing to set and get keys from Redis.
The library exposes 2 functions RedisSet and RedisGet. Both function are called from the corresponding routines.

## Installation

The library depends on the official C Redis library: https://github.com/redis/hiredis


```bash
# Clone this repository
git clone https://gitlab.com/nsitbim/satellites/rtredis.git
cd RtRedis
cd lib
# Get the Redis library
git clone -b v1.0.0 https://github.com/redis/hiredis.git
cd hiredis
# Compile
make
# Install
cp libhiredis.so /usr/lib/
ldconfig
cd ../..
# Compile the routine library
make
cp lib/libRtRedis.so /opt/IBM/InformationServer/Server/PXEngine/user_lib/
```

Commands and MakeFile might need to be adapted to your system. This was tested with Datastage 11.7.1 on RedHat.

Last step is to import the isx file and change the library path if needed. And add the REDIS_HOST and REDIS_PORT environment variables.

## Usage

In a parallel Transformer
```
RedisSet("key","value","") //Set key=value
RedisSet("key","value","EX 60") //Set key=value that expires in 1 minute
RedisGet("key") //Retrieve the value for key
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)